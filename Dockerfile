#STAGE I
FROM python:3.7-slim AS COMPILE_IMAGE
RUN apt-get update
RUN apt-get install -y --no-install-recommends build-essential gcc && pip3 install --upgrade pip

WORKDIR /app
COPY . /app

RUN pip3 --no-cache-dir install -r requirements.txt

CMD ["flask", "run", "--host=0.0.0.0"]
