import os
from flask import Flask
from flask_restful import Resource, Api

from app.controllers.plates_id_controller import PlatesIdController
from app.controllers.plates_number_controller import PlatesNumberController
from app.controllers.matrix_controller import MatrixController


app = Flask(__name__)

api = Api(app)

class HealthCheck(Resource):
    def get(self):
        return {'healthcheck': 'server running'}


api.add_resource(HealthCheck, "/")
api.add_resource(PlatesIdController, "/getid/<plate>")
api.add_resource(PlatesNumberController, "/getplate/<id>")
api.add_resource(MatrixController, "/matrixsum/<r>/<c>/<z>/<x>/<y>")


if __name__ == '__main__':
    app.run()  