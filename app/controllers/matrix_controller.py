from flask import request, jsonify
from flask_restful import Resource

class MatrixController(Resource):
    '''
    Controller that process numeric parameters to resolve matrix members addition
    '''
    def get(self, r, c, z, x, y):
        '''
        Method that accepts five parameters, (r,c -> matrix's rows and columns, z -> scalar value, x, y -> coordinates)
        and returns the sum of values within the specidied coordinates
        '''
        try:
            r = int(r)
            c = int(c)
            z = int(z)
            x = int(x)
            y = int(y)
            # initialize matrix
            matrix = [[0 for col in range(c)] for row in range(r)] 
            
            for row in range(0, r):
                for col in range(0, c):
                    if row == 0:
                        matrix[row][col] = z
                    else:
                        matrix[row][col] = z + (matrix[row-1][col] - 1)
                        
            print(matrix)   

            tot = 0
            for y_cord in range(0, y+1):
                sum = 0
                for x_cord in range(0, x+1):
                    sum += matrix[y_cord][x_cord]
                    print(matrix[y_cord][x_cord])
                tot += sum
            print('suma es {}'.format(tot)) 
            return tot, 200
        except:
            return {'Error': 'Ooops, hubo un error! formato de variables invalido'}
    

