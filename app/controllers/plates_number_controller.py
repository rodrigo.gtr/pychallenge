from flask import request, jsonify
from flask_restful import Resource

class PlatesNumberController(Resource):
    '''
    Controller that process car plates queries
    '''
    def get(self, id):
        id_number = int(id)
        letters_index = 'abcdefghijklmnopqrstuvwxyz'

        letters = [0,0,0,0]
        if id_number > 999:
            plate_number = str(id_number)[-3:]
            rest = id_number

            for l in range(0,4):
                if rest/((26**(3-l))*(1000)) >= 1:
                    if l == 3:
                        index = (rest - (((26**(3-l)))) )/(26**(3-l)*1000)
                        if index > 1:
                            rest -= int(index)*1000 + 1
                        else:
                            rest -= (26**(3-l))*index*1000 -998
                        let = letters_index[int(index)]
                        letters[l] = let.upper()
                    else:
                        index = (rest - (((26**(3-l))*1000)))/(26**(3-l)*1000)
                        rest = rest - (26**(3-l))*index*1000
                        let = letters_index[int(index)]
                        letters[l] = let.upper()
                else:
                    letters[l] = 'A'
            if int(rest) < 10:
                out_plate = letters.append('0'+'0'+str(int(rest)))
            elif rest > 10 and rest < 100:
                out_plate = letters.append('0'+str(int(rest)))
            elif rest > 100:
                out_plate = letters.append(int(rest))
            print('Plate is {}'.format( ''.join(map(str, letters))))
            return ('Plate is {}'.format( ''.join(map(str, letters)))), 200
        else: 
            rest = id_number
            if int(rest) < 10:
                out_plate = '0'+'0'+str(int(rest-1))
            elif rest > 10 and rest < 100:
                out_plate = '0'+str(int(rest-1))
            else:
                out_plate = int(rest-1)
            print('Plate is AAAA{}'.format(out_plate))
            plate = 'AAAA'+str(out_plate)
            return (plate , 200)
