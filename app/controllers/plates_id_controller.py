from flask import request, jsonify
from flask_restful import Resource
import re


class PlatesIdController(Resource):
    '''
    Controller that process car id queries
    '''
    def get(self, plate):
        '''
        This method returns the corresponding user_id to the plate number provided
        '''
        check_plate = re.match('[a-z]{4}\d{3}', plate.lower())
        if check_plate:
            literal = (plate[0:4]).lower()
            print(len(literal))
            print('literal es {}'.format(literal))
            numbers = int(plate[4:])
            letters_index = 'abcdefghijklmnopqrstuvwxyz'

            sum = 0
            for f in range(0, len(literal)):
                factor = (letters_index.index(literal[f])) * 26**(len(literal) -1 - f) * 1000
                print(factor)
                sum += factor
            print('el ID de la placa {} es {}'.format(plate, sum+numbers+1))
            id_out = sum+numbers+1
            return str(id_out), 200
        else:
            return {'Error': 'Ooops, hubo un error! formato de patente no valido'}
