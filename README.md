# Patentes y Matriz

Este es una servicio REST que entrega la ID de usuario basado en una patente ingresada en el 
formato 'AAAA000', así como también la patente asociada a un ID de usuario en particular. 
Además calcula la sumatoria de una matriz a partir de parámetros numéricos ingresados.
Se provee un archivo Docker mínimo para ejecutar el servicio en distintas plataformas.

Los endpoints disponibles son los siguientes:

- getid/<numero_de_patente>
- getplate/<id>
- matrixsum/<r>/<c>/<z>/<x>/<y>

Para ocupar este servicio, se requiere lo siguiente

1. Clonar este proyecto en directorio de preferencia utilizando 'git clone https://gitlab.com/rodrigo.gtr/pychallenge.git'
2. Para facilitar la compatibilidad entre plataformas se recumienda utilizar Docker
3. Una vez descargado Docker, posicionarse en directorio de la aplicación y ejecutar 'docker build -t server:1.0'
4. luego levantar el container con comando 'docker run -p 5000:5000 server:1.0'
5. probar los endpoints en 127.0.0.1:5000

Para testear el servicio, ejecutar 'pytest' en el directorio raiz