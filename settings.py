from dotenv import load_dotenv
load_dotenv()

from setuptools import setup

setup(
    name='app',
    packages=['app'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)
