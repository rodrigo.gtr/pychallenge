from app.controllers.plates_id_controller import PlatesIdController

plate1 = 'AAAA000'
plate2 = 'AAAA001'
plate3 = 'AAAA002'
plate4 = 'AAAB000'
plate5 = 'AAAB001'
plate6 = 'AAAB002'
plate7 = 'AAAA999'
plate8 = 'ZZZZ999'

def test_plates():

    getId = PlatesIdController()

    expected = '1', 200
    actual = getId.get(plate1)
    assert expected == actual

    expected = '2', 200
    actual = getId.get(plate2)
    assert expected == actual

    expected = '3', 200
    actual = getId.get(plate3)
    assert expected == actual

    expected = '1001', 200
    actual = getId.get(plate4)
    assert expected == actual

    expected = '1002', 200
    actual = getId.get(plate5)
    assert expected == actual

    expected = '1003', 200
    actual = getId.get(plate6)
    assert expected == actual
    
    expected = '1000', 200
    actual = getId.get(plate7)
    assert expected == actual

    expected = '456976000', 200 
    actual = getId.get(plate8)
    assert expected == actual