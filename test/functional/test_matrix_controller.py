from app.controllers.matrix_controller import MatrixController

def test_matrix_sum():

    get_sum = MatrixController

    expected = 18, 200
    actual = get_sum.get('s', 4, 3, 2, 1, 2)
    assert actual == expected

    expected = 370, 200
    actual = get_sum.get('s', 5, 6, 8, 4, 3)
    assert actual == expected